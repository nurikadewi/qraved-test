package com.nurikadewi.qravedtest.di.components;

import com.nurikadewi.qravedtest.di.module.DetailMovieModule;
import com.nurikadewi.qravedtest.di.module.DiscoverModule;
import com.nurikadewi.qravedtest.di.scope.PerActivity;
import com.nurikadewi.qravedtest.modules.detailmovie.DetailMovieActivity;
import com.nurikadewi.qravedtest.modules.discover.DiscoverActivity;

import dagger.Component;


/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

@PerActivity
@Component(modules = DiscoverModule.class, dependencies = IAppComponent.class)
public interface IDiscoverComponent {

    void inject(DiscoverActivity activity);
}
