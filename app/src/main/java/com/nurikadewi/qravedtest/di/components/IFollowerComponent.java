package com.nurikadewi.qravedtest.di.components;

import com.nurikadewi.qravedtest.di.module.FollowerModule;
import com.nurikadewi.qravedtest.di.scope.PerActivity;
import com.nurikadewi.qravedtest.modules.follower.FollowerActivity;

import dagger.Component;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

@PerActivity
@Component(modules = FollowerModule.class, dependencies = IAppComponent.class)
public interface IFollowerComponent {

    void inject(FollowerActivity activity);
}
