package com.nurikadewi.qravedtest.di.module;

import com.nurikadewi.qravedtest.api.IApiService;
import com.nurikadewi.qravedtest.di.scope.PerActivity;
import com.nurikadewi.qravedtest.mvp.view.IFollowerView;
import com.nurikadewi.qravedtest.mvp.view.IMainView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

@Module
public class MainModule {

    private IMainView mView;

    public MainModule(IMainView view) {
        mView = view;
    }

    @PerActivity
    @Provides
    IApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(IApiService.class);
    }

    @PerActivity
    @Provides
    IMainView provideView() {
        return mView;
    }
}
