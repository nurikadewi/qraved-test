package com.nurikadewi.qravedtest.di.components;

import com.nurikadewi.qravedtest.di.module.DetailMovieModule;
import com.nurikadewi.qravedtest.di.scope.PerActivity;
import com.nurikadewi.qravedtest.modules.detailmovie.DetailMovieActivity;

import dagger.Component;


/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

@PerActivity
@Component(modules = DetailMovieModule.class, dependencies = IAppComponent.class)
public interface IDetailMovieComponent {

    void inject(DetailMovieActivity activity);
}
