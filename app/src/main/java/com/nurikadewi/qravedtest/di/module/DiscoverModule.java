package com.nurikadewi.qravedtest.di.module;

import com.nurikadewi.qravedtest.api.IApiService;
import com.nurikadewi.qravedtest.di.scope.PerActivity;
import com.nurikadewi.qravedtest.mvp.view.IDetailMovieView;
import com.nurikadewi.qravedtest.mvp.view.IDiscoverView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

@Module
public class DiscoverModule {

    private IDiscoverView mView;

    public DiscoverModule(IDiscoverView view) {
        mView = view;
    }

    @PerActivity
    @Provides
    IApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(IApiService.class);
    }

    @PerActivity
    @Provides
    IDiscoverView provideView() {
        return mView;
    }
}
