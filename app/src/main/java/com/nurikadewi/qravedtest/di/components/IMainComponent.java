package com.nurikadewi.qravedtest.di.components;

import com.nurikadewi.qravedtest.di.module.FollowerModule;
import com.nurikadewi.qravedtest.di.module.MainModule;
import com.nurikadewi.qravedtest.di.scope.PerActivity;
import com.nurikadewi.qravedtest.modules.follower.FollowerActivity;
import com.nurikadewi.qravedtest.modules.main.MainActivity;

import dagger.Component;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

@PerActivity
@Component(modules = MainModule.class, dependencies = IAppComponent.class)
public interface IMainComponent {

    void inject(MainActivity activity);
}
