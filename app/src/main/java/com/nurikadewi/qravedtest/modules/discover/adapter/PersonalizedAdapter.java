package com.nurikadewi.qravedtest.modules.discover.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nurikadewi.qravedtest.R;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class PersonalizedAdapter extends BaseAdapter {
    private Context mContext;
    private final String[] mImg;

    public PersonalizedAdapter(Context c, String[] mImg) {
        mContext = c;
        this.mImg = mImg;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mImg.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.list_item_presonalized, null);
            ImageView imageView = (ImageView) grid.findViewById(R.id.grid_image);

            Glide.with(mContext).load(mImg[position]).into(imageView);
        } else {
            grid = (View) convertView;
        }

        return grid;
    }
}
