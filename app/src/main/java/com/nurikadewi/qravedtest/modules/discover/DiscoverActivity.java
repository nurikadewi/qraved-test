package com.nurikadewi.qravedtest.modules.discover;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nurikadewi.qravedtest.R;
import com.nurikadewi.qravedtest.base.BaseActivity;
import com.nurikadewi.qravedtest.di.components.DaggerIDiscoverComponent;
import com.nurikadewi.qravedtest.di.module.DiscoverModule;
import com.nurikadewi.qravedtest.modules.discover.adapter.PersonalizedAdapter;
import com.nurikadewi.qravedtest.mvp.model.DiscoverResponse;
import com.nurikadewi.qravedtest.mvp.presenter.DiscoverPresenter;
import com.nurikadewi.qravedtest.mvp.view.IDiscoverView;
import com.nurikadewi.qravedtest.utilities.NetworkUtils;

import javax.inject.Inject;

import butterknife.Bind;

import static android.R.attr.thumb;

public class DiscoverActivity extends BaseActivity implements IDiscoverView {

    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;
    @Bind(R.id.text_title_trending)
    protected TextView mTextTitleTrending;
    @Bind(R.id.text_title_interesting)
    protected TextView mTextTitleInter;
    @Bind(R.id.text_title_personalized)
    protected TextView mTextTitlePersonal;
    @Bind(R.id.trending_groups_layout)
    protected LinearLayout mLayTrending;
    @Bind(R.id.inte_user_layout)
    protected LinearLayout mLayInters;
    @Bind(R.id.grid_presonalcontent)
    protected GridView mGridPesonal;
    @Inject
    protected DiscoverPresenter mPresenter;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        initializeContactInfo();
        loadDiscover();
    }

    private void loadDiscover() {
        if (NetworkUtils.isNetAvailable(this)) {
            mPresenter.getDiscover();
        } else {
            mPresenter.onErrorNetwork();
        }
    }

    private void initializeContactInfo() {
    }

    @Override
    public int getContentView() {
        return R.layout.activity_discover;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_discover, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void resolveDaggerDependency() {
        DaggerIDiscoverComponent.builder()
                .iAppComponent(getApplicationComponent())
                .discoverModule(new DiscoverModule(this))
                .build().inject(this);
    }

    @Override
    public void onDicoverLoaded(DiscoverResponse response) {
        showToolbar(mToolbar, R.drawable.ic_back_black, getString(R.string.title_activity_discover));

        mTextTitleTrending.setText(response.getDiscover().getTrending_groups().getTitle());
        for (DiscoverResponse.Discover.TrendingGroups.ListTrending trendingGroups : response.getDiscover().getTrending_groups().getList()) {
            final View layout = View.inflate(this, R.layout.list_item_trending_user, null);
            ImageView imgPic = (ImageView) layout.findViewById(R.id.img_picture);
            ImageView imgIco = (ImageView) layout.findViewById(R.id.img_icon);
            TextView textName = (TextView) layout.findViewById(R.id.text_name);
            Glide.with(this).load(trendingGroups.getPicture_group()).into(imgPic);
            Glide.with(this).load(trendingGroups.getIcon_group()).into(imgIco);
            textName.setText(trendingGroups.getTitle_group());
            mLayTrending.addView(layout);
        }

        mTextTitleInter.setText(response.getDiscover().getInteristing_users().getTitle());
        for (DiscoverResponse.Discover.InteristingUsers.Picture picture : response.getDiscover().getInteristing_users().getPicture()) {
            final View layout = View.inflate(this, R.layout.list_item_interes, null);
            ImageView imgPic = (ImageView) layout.findViewById(R.id.img_pic_user);
            Glide.with(getApplicationContext()).load(picture.getUrl_picture()).apply(RequestOptions.circleCropTransform()).into(imgPic);
            mLayInters.addView(layout);
        }

        mTextTitlePersonal.setText(response.getDiscover().getPersonalized_content().getTitle());
        String[] img = new String[response.getDiscover().getPersonalized_content().getPicture().size()];
        int i = 0;
        for (DiscoverResponse.Discover.PersonalizedContent.PictureX picture : response.getDiscover().getPersonalized_content().getPicture()) {
            img[i] = picture.getUrl_image();
            i++;
        }
        PersonalizedAdapter adapter = new PersonalizedAdapter(DiscoverActivity.this, img);
        mGridPesonal.setAdapter(adapter);

    }

    @Override
    public void onShowDialog(String message) {
        showDialog(message);
    }

    @Override
    public void onShowAlertDialog(String title, String message) {
        showAlertDialog(title, message);
    }

    @Override
    public void onHideDialog() {
        hideDialog();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(DiscoverActivity.this, message, Toast.LENGTH_SHORT).show();
    }

}
