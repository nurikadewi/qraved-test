package com.nurikadewi.qravedtest.modules.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.nurikadewi.qravedtest.R;
import com.nurikadewi.qravedtest.base.BaseActivity;
import com.nurikadewi.qravedtest.di.components.DaggerIFollowerComponent;
import com.nurikadewi.qravedtest.di.components.DaggerIMainComponent;
import com.nurikadewi.qravedtest.di.module.FollowerModule;
import com.nurikadewi.qravedtest.di.module.MainModule;
import com.nurikadewi.qravedtest.modules.detailmovie.DetailMovieActivity;
import com.nurikadewi.qravedtest.modules.discover.DiscoverActivity;
import com.nurikadewi.qravedtest.modules.follower.FollowerActivity;
import com.nurikadewi.qravedtest.mvp.view.IBaseView;
import com.nurikadewi.qravedtest.mvp.view.IMainView;

import javax.inject.Inject;

import butterknife.Bind;

public class MainActivity extends BaseActivity implements IMainView, View.OnClickListener {

    @Bind(R.id.btn_discover)
    protected Button mBtnDiscover;
    @Bind(R.id.btn_follower)
    protected Button mBtnFollower;
    @Bind(R.id.btn_detail_movie)
    protected Button mBtnDetailMovie;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        mBtnDiscover.setOnClickListener(this);
        mBtnFollower.setOnClickListener(this);
        mBtnDetailMovie.setOnClickListener(this);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void resolveDaggerDependency() {
        DaggerIMainComponent.builder()
                .iAppComponent(getApplicationComponent())
                .mainModule(new MainModule(this))
                .build().inject(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_discover:
                startActivity(new Intent(this, DiscoverActivity.class));
                break;
            case R.id.btn_follower:
                startActivity(new Intent(this, FollowerActivity.class));
                break;
            case R.id.btn_detail_movie:
                startActivity(new Intent(this, DetailMovieActivity.class));
                break;
        }
    }
}
