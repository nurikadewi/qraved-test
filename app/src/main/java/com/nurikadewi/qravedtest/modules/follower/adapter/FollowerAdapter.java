package com.nurikadewi.qravedtest.modules.follower.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nurikadewi.qravedtest.R;
import com.nurikadewi.qravedtest.mvp.model.FollowerResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.Holder> {

    private LayoutInflater mLayoutInflater;
    private List<FollowerResponse.Follower> mFollowerResponseList = new ArrayList<>();

    public FollowerAdapter(LayoutInflater inflater) {
        mLayoutInflater = inflater;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_followers, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(mFollowerResponseList.get(position));
    }

    @Override
    public int getItemCount() {
        return mFollowerResponseList.size();
    }

    public void addFollower(List<FollowerResponse.Follower> followerResponses) {
        mFollowerResponseList.addAll(followerResponses);
        notifyDataSetChanged();
    }

    public void clearContact() {
        mFollowerResponseList.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected TextView mTextInitial;
        @Bind(R.id.text_contact_fname)
        protected TextView mTextFollowerFname;
        @Bind(R.id.text_contact_lname)
        protected TextView mTextFollowerUname;
        @Bind(R.id.img_user)
        protected ImageView mImgUser;
        @Bind(R.id.btn_follow)
        protected Button mBtnFollow;

        private Context mContext;
        private FollowerResponse.Follower mFollowerResponse;

        public Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mContext = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("NewApi")
        public void bind(FollowerResponse.Follower follower) {
            mFollowerResponse = follower;
            mTextFollowerFname.setText(follower.getFullname());
            mTextFollowerUname.setText(follower.getUsername());

            Glide.with(mContext).load(follower.getIcon()).apply(RequestOptions.circleCropTransform()).into(mImgUser);

            if (follower.isFollowing()) {
                mBtnFollow.setText(mContext.getResources().getString(R.string.follow));
                mBtnFollow.setBackground(mContext.getResources().getDrawable(R.drawable.bg_btn_follow));
            } else {
                mBtnFollow.setText(mContext.getResources().getString(R.string.following));
                mBtnFollow.setBackground(mContext.getResources().getDrawable(R.drawable.bg_btn_following));
            }


        }

        @Override
        public void onClick(View v) {
            if (mContactClickListener != null) {
                mContactClickListener.onClick(mTextInitial, mFollowerResponse, getAdapterPosition());
            }
        }
    }

    public void setContactClickListener(OnContactClickListener listener) {
        mContactClickListener = listener;
    }

    private OnContactClickListener mContactClickListener;

    public interface OnContactClickListener {

        void onClick(View v, FollowerResponse.Follower follower, int position);
    }
}
