package com.nurikadewi.qravedtest.modules.detailmovie;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nurikadewi.qravedtest.R;
import com.nurikadewi.qravedtest.base.BaseActivity;
import com.nurikadewi.qravedtest.di.components.DaggerIDetailMovieComponent;
import com.nurikadewi.qravedtest.di.module.DetailMovieModule;
import com.nurikadewi.qravedtest.mvp.model.MovieDetailResponse;
import com.nurikadewi.qravedtest.mvp.presenter.DetailPresenter;
import com.nurikadewi.qravedtest.mvp.view.IDetailMovieView;
import com.nurikadewi.qravedtest.utilities.NetworkUtils;

import javax.inject.Inject;

import butterknife.Bind;

public class DetailMovieActivity extends BaseActivity implements IDetailMovieView, View.OnClickListener {

    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;
    @Bind(R.id.img_cover_movie)
    protected ImageView mImgCover;
    @Bind(R.id.text_title)
    protected TextView mTextTitle;
    @Bind(R.id.text_year)
    protected TextView mTextYear;
    @Bind(R.id.text_genre)
    protected TextView mTextGenre;
    @Bind(R.id.text_sinopsis)
    protected TextView mTextSinopsis;
    @Bind(R.id.text_rating)
    protected TextView mTextRating;
    @Bind(R.id.thumb_layout)
    protected LinearLayout mThumbLayout;
    @Bind(R.id.cast_layout)
    protected LinearLayout mCastLayout;
    @Bind(R.id.app_bar)
    protected AppBarLayout mAppBar;
    @Bind(R.id.hscroll_cast)
    protected HorizontalScrollView mHScrollCast;
    @Bind(R.id.btn_prev_cast)
    protected ImageButton mBtnPrevCast;
    @Bind(R.id.btn_next_cast)
    protected ImageButton mBtnNextCast;
    @Bind(R.id.hscroll_thumb)
    protected HorizontalScrollView mHScrollThumb;
    @Bind(R.id.btn_prev_thumb)
    protected ImageButton mBtnPrevThumb;
    @Bind(R.id.btn_next_thumb)
    protected ImageButton mBtnNextThumb;
    @Inject
    protected DetailPresenter mPresenter;
    private int maxScrollX;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        initializeContactInfo();
        loadMovie();
    }

    private void loadMovie() {
        if (NetworkUtils.isNetAvailable(this)) {
            mPresenter.getMovie();
        } else {
            mPresenter.onErrorNetwork();
        }
    }

    private void initializeContactInfo() {
    }

    @Override
    public int getContentView() {
        return R.layout.activity_detail_movie;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void resolveDaggerDependency() {
        DaggerIDetailMovieComponent.builder()
                .iAppComponent(getApplicationComponent())
                .detailMovieModule(new DetailMovieModule(this))
                .build().inject(this);
    }

    @Override
    public void onMovieLoaded(MovieDetailResponse response) {
        showToolbar(mToolbar, R.drawable.ic_back, "");
        Glide.with(this).load(response.getDetail_movie().getCover()).into(mImgCover);
        mTextTitle.setText(response.getDetail_movie().getTitle());
        mTextYear.setText("(" + response.getDetail_movie().getYear() + ")");
        mTextGenre.setText(response.getDetail_movie().getGenre());
        mTextSinopsis.setText(response.getDetail_movie().getSinopsis());
        mTextRating.setText(response.getDetail_movie().getRating());

        for (MovieDetailResponse.DetailMovie.ThumbPicture thumb : response.getDetail_movie().getThumb_picture()) {
            final View thumbLayout = View.inflate(this, R.layout.list_item_thum_picture, null);
            ImageView imgThumb = (ImageView) thumbLayout.findViewById(R.id.img_thum_picture);
            Glide.with(this).load(thumb.getUrl_image()).into(imgThumb);
            mThumbLayout.addView(thumbLayout);
        }

        for (MovieDetailResponse.DetailMovie.Cast cast : response.getDetail_movie().getCast()) {
            final View castLayout = View.inflate(this, R.layout.list_item_cast_picture, null);
            ImageView imgCast = (ImageView) castLayout.findViewById(R.id.img_cast_picture);
            TextView textNameCast = (TextView) castLayout.findViewById(R.id.text_cast_name);
            Glide.with(this).load(cast.getPic_cast()).into(imgCast);
            textNameCast.setText(cast.getName_cast());
            mCastLayout.addView(castLayout);
        }

        initializeHScroll();
    }

    @SuppressLint("NewApi")
    private void initializeHScroll() {

        mBtnPrevCast.setOnClickListener(this);
        mBtnPrevCast.setVisibility(View.INVISIBLE);
        mBtnNextCast.setOnClickListener(this);
        mBtnPrevThumb.setOnClickListener(this);
        mBtnPrevThumb.setVisibility(View.INVISIBLE);
        mBtnNextThumb.setOnClickListener(this);

        mHScrollThumb.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Log.i("Scroll ", scrollX + " " + scrollY + " " + oldScrollX + " " + oldScrollY);
                if (scrollX > getWindowManager().getDefaultDisplay().getWidth()
                        && scrollY <= getWindowManager().getDefaultDisplay().getWidth()) {
                    mBtnPrevThumb.setVisibility(View.VISIBLE);
                    mBtnNextThumb.setVisibility(View.INVISIBLE);
                } else if (scrollX == 0) {
                    mBtnPrevThumb.setVisibility(View.INVISIBLE);
                    mBtnNextThumb.setVisibility(View.VISIBLE);
                } else {
                    mBtnPrevThumb.setVisibility(View.VISIBLE);
                    mBtnNextThumb.setVisibility(View.VISIBLE);
                }
            }
        });
        mHScrollCast.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Log.i("Scroll ", scrollX + " " + scrollY + " " + oldScrollX + " " + oldScrollY);
                if (scrollX > getWindowManager().getDefaultDisplay().getWidth()
                        && scrollY <= getWindowManager().getDefaultDisplay().getWidth()) {
                    mBtnPrevCast.setVisibility(View.VISIBLE);
                    mBtnNextCast.setVisibility(View.INVISIBLE);
                } else if (scrollX == 0) {
                    mBtnPrevCast.setVisibility(View.INVISIBLE);
                    mBtnNextCast.setVisibility(View.VISIBLE);
                } else {
                    mBtnPrevCast.setVisibility(View.VISIBLE);
                    mBtnNextCast.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onShowDialog(String message) {
        showDialog(message);
    }

    @Override
    public void onShowAlertDialog(String title, String message) {
        showAlertDialog(title, message);
    }

    @Override
    public void onHideDialog() {
        hideDialog();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(DetailMovieActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onScrollingImage(final ImageButton btn, final HorizontalScrollView hs, final int action) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                switch (action) {
                    case 0: //for next
                        int x = (int) hs.getScrollX() + getWindowManager().getDefaultDisplay().getWidth();
                        int y = (int) hs.getScrollY();
                        hs.smoothScrollTo(x, y);
                        break;
                    case 1: //for prev
                        int xx = (int) hs.getScrollX() - getWindowManager().getDefaultDisplay().getWidth();
                        int yy = (int) hs.getScrollY();
                        hs.smoothScrollTo(xx, yy);
                        break;
                }
            }
        }, 100L);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next_cast:
                onScrollingImage(mBtnNextCast, mHScrollCast, 0);
                break;
            case R.id.btn_next_thumb:
                onScrollingImage(mBtnNextThumb, mHScrollThumb, 0);
                break;
            case R.id.btn_prev_cast:
                onScrollingImage(mBtnPrevCast, mHScrollCast, 1);
                break;
            case R.id.btn_prev_thumb:
                onScrollingImage(mBtnPrevThumb, mHScrollThumb, 1);
                break;
        }
    }
}
