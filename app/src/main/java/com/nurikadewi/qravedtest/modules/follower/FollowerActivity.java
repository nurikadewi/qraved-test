package com.nurikadewi.qravedtest.modules.follower;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nurikadewi.qravedtest.R;
import com.nurikadewi.qravedtest.base.BaseActivity;
import com.nurikadewi.qravedtest.di.components.DaggerIFollowerComponent;
import com.nurikadewi.qravedtest.di.module.FollowerModule;
import com.nurikadewi.qravedtest.modules.follower.adapter.FollowerAdapter;
import com.nurikadewi.qravedtest.mvp.model.FollowerResponse;
import com.nurikadewi.qravedtest.mvp.presenter.FollowerPresenter;
import com.nurikadewi.qravedtest.mvp.view.IFollowerView;
import com.nurikadewi.qravedtest.utilities.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

public class FollowerActivity extends BaseActivity implements IFollowerView, View.OnClickListener {

    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;
    @Bind(R.id.rv_follower_list)
    protected RecyclerView mFollowerList;
    @Bind(R.id.text_no_follower)
    protected TextView mNoFollower;
    @Inject
    protected FollowerPresenter mPresenter;
    private FollowerAdapter mFollowerAdapter;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        showToolbar(mToolbar, R.drawable.ic_back_black, getString(R.string.title_activity_follower));
        initializeList();
        loadContacts();
    }

    private void loadContacts() {
        if (NetworkUtils.isNetAvailable(this)) {
            mPresenter.getContacts();
        } else {
            mPresenter.onErrorNetwork();
        }
    }

    private void initializeList() {
        mFollowerList.setHasFixedSize(true);
        mFollowerList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mFollowerAdapter = new FollowerAdapter(getLayoutInflater());
        mFollowerAdapter.setContactClickListener(mContactClickListener);
        mFollowerList.setAdapter(mFollowerAdapter);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_follower;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_follower, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void resolveDaggerDependency() {
        DaggerIFollowerComponent.builder()
                .iAppComponent(getApplicationComponent())
                .followerModule(new FollowerModule(this))
                .build().inject(this);
    }

    @Override
    public void onFollowersLoaded(List<FollowerResponse.Follower> followerResponses) {
        if (followerResponses.size() == 0) {
            mNoFollower.setVisibility(View.VISIBLE);
            mFollowerList.setVisibility(View.GONE);
        } else {
            mFollowerList.setVisibility(View.VISIBLE);
            mNoFollower.setVisibility(View.GONE);
            mFollowerAdapter.addFollower(followerResponses);
        }
    }

    @Override
    public void onShowDialog(String message) {
        showDialog(message);
    }

    @Override
    public void onShowAlertDialog(String title, String message) {
        showAlertDialog(title, message);
    }

    @Override
    public void onHideDialog() {
        hideDialog();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(FollowerActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClearItems() {
        mFollowerAdapter.clearContact();
    }

    @Override
    public void onClick(View view) {

    }

    private FollowerAdapter.OnContactClickListener mContactClickListener = new FollowerAdapter.OnContactClickListener() {
        @Override
        public void onClick(View v, FollowerResponse.Follower followerResponse, int position) {

        }
    };

}
