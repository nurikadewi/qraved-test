package com.nurikadewi.qravedtest.mvp.view;

import android.content.Intent;

import com.nurikadewi.qravedtest.mvp.model.FollowerResponse;

import java.util.List;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public interface IFollowerView extends IBaseView {

    void onFollowersLoaded(List<FollowerResponse.Follower> response);

    void onShowDialog(String message);

    void onShowAlertDialog(String title, String message);

    void onHideDialog();

    void onShowToast(String message);

    void onClearItems();
}