package com.nurikadewi.qravedtest.mvp.model;

import java.util.List;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class FollowerResponse {

    private List<Follower> follower;

    public List<Follower> getFollower() {
        return follower;
    }

    public void setFollowerResponse(List<Follower> follower) {
        this.follower = follower;
    }

    public static class Follower {
        /**
         * username : @nurikadewi
         * icon : https://image.ibb.co/dqwDcF/images.jpg
         * fullname : Nurika Dewi
         * following : true
         */

        private String username;
        private String icon;
        private String fullname;
        private boolean following;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public boolean isFollowing() {
            return following;
        }

        public void setFollowing(boolean following) {
            this.following = following;
        }
    }
}
