package com.nurikadewi.qravedtest.mvp.view;

import com.nurikadewi.qravedtest.mvp.model.DiscoverResponse;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public interface IDiscoverView extends IBaseView {

    void onDicoverLoaded(DiscoverResponse response);

    void onShowDialog(String message);

    void onShowAlertDialog(String title, String message);

    void onHideDialog();

    void onShowToast(String message);
}
