package com.nurikadewi.qravedtest.mvp.view;

import android.widget.HorizontalScrollView;
import android.widget.ImageButton;

import com.nurikadewi.qravedtest.mvp.model.MovieDetailResponse;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public interface IDetailMovieView extends IBaseView {

    void onMovieLoaded(MovieDetailResponse response);

    void onShowDialog(String message);

    void onShowAlertDialog(String title, String message);

    void onHideDialog();

    void onShowToast(String message);

    void onScrollingImage(ImageButton btn, HorizontalScrollView hs, int action);
}
