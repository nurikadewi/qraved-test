package com.nurikadewi.qravedtest.mvp.presenter;

import com.nurikadewi.qravedtest.api.IApiService;
import com.nurikadewi.qravedtest.base.BasePresenter;
import com.nurikadewi.qravedtest.mvp.model.MovieDetailResponse;
import com.nurikadewi.qravedtest.mvp.view.IDetailMovieView;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class DetailPresenter extends BasePresenter<IDetailMovieView> implements Observer<MovieDetailResponse> {

    @Inject
    protected IApiService mApiService;

    @Inject
    protected DetailPresenter() {
    }

    public void getMovie() {
        getView().onShowDialog("Please Wait..");
        Observable<MovieDetailResponse> ContactsResponseObservable = mApiService.getMovieDetail();
        subscribe(ContactsResponseObservable, this);
    }

    @Override
    public void onCompleted() {
        getView().onHideDialog();
    }

    @Override
    public void onError(Throwable e) {
        getView().onHideDialog();
        getView().onShowToast("Error loading data : " + e.getMessage());
    }

    @Override
    public void onNext(MovieDetailResponse response) {
        getView().onMovieLoaded(response);
    }

    public void onErrorNetwork() {
        getView().onShowAlertDialog("Network Error", "Unable to contact the server");
    }
}
