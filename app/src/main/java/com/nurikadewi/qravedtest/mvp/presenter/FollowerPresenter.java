package com.nurikadewi.qravedtest.mvp.presenter;

import com.nurikadewi.qravedtest.api.IApiService;
import com.nurikadewi.qravedtest.base.BasePresenter;
import com.nurikadewi.qravedtest.mvp.model.FollowerResponse;
import com.nurikadewi.qravedtest.mvp.view.IFollowerView;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class FollowerPresenter extends BasePresenter<IFollowerView> implements Observer<FollowerResponse> {

    @Inject
    protected IApiService mApiService;

    @Inject
    protected FollowerPresenter() {
    }

    public void getContacts() {
        getView().onShowDialog("Please Wait..");
        Observable<FollowerResponse> ContactsResponseObservable = mApiService.getFollowers();
        subscribe(ContactsResponseObservable, this);
    }

    @Override
    public void onCompleted() {
        getView().onHideDialog();
    }

    @Override
    public void onError(Throwable e) {
        getView().onHideDialog();
        getView().onShowToast("Error loading data : " + e.getMessage());
    }

    @Override
    public void onNext(FollowerResponse response) {
        getView().onClearItems();
        getView().onFollowersLoaded(response.getFollower());
    }

    public void onErrorNetwork() {
        getView().onShowAlertDialog("Network Error", "Unable to contact the server");
    }
}
