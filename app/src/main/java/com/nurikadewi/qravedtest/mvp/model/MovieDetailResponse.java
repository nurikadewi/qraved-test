package com.nurikadewi.qravedtest.mvp.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class MovieDetailResponse implements Serializable {


    /**
     * detail_movie : {"title":"Inception","year":"2010","cover":"https://image.ibb.co/kYbtBa/inception_sequel_cast.jpg","genre":"Action, Sci-fi, Mystery, Thriller","sinopsis":"Dominic Dom Cobb (Leonardo DiCaprio) dan rekannya, Arthur (Joseph Gordon-Levitt) mempraktikkan spionase ilegal dengan memasuki alam bawah sadar target mereka, dan menggunakan strategi dua tingkat mimpi ('mimpi dalam mimpi') untuk mengekstrak informasi penting yang dibutuhkan. Setiap ekstraktor membawa sebuah totem, benda pribadi yang berukuran kecil. Totem milik Cobb masih belum jelas namun ada kemungkinan bahwa cincin kawinnyalah yang menjadi totem miliknya, dikarenakan gasing yang ada di tangannya adalah totem milik istrinya. Cobb berjuang bersama memori istrinya yang telah meninggal, Mal (Marion Cotillard), yang memanifestasi mimpinya serta berusaha untuk menyabotase kehidupannya.","thumb_picture":[{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"}],"trailer_url":"https://www.youtube.com/watch?v=8hP9D6kZseM","rating":"8.7","award":{"green":true,"red":false,"teal":true,"violet":false},"cast":[{"name_cast":"Leonardo DiCaprio","pic_cast":"https://image.ibb.co/hYfZjv/leonardo_dicaprio.jpg"},{"name_cast":"Tom Hardy","pic_cast":"https://image.ibb.co/ek8n4v/tom_hardy.jpg"},{"name_cast":"Ellen Page","pic_cast":"https://image.ibb.co/k5gwxF/ellen_page.jpg"},{"name_cast":"Joseph Gordon-Levitt","pic_cast":"https://image.ibb.co/iF7rWa/joshep_gordon.jpg"},{"name_cast":"Marion Cotillard","pic_cast":"https://image.ibb.co/mwg74v/marion_cotillard.jpg"}]}
     */

    private DetailMovie detail_movie;

    public DetailMovie getDetail_movie() {
        return detail_movie;
    }

    public void setDetail_movie(DetailMovie detail_movie) {
        this.detail_movie = detail_movie;
    }

    public static class DetailMovie {
        /**
         * title : Inception
         * year : 2010
         * cover : https://image.ibb.co/kYbtBa/inception_sequel_cast.jpg
         * genre : Action, Sci-fi, Mystery, Thriller
         * sinopsis : Dominic Dom Cobb (Leonardo DiCaprio) dan rekannya, Arthur (Joseph Gordon-Levitt) mempraktikkan spionase ilegal dengan memasuki alam bawah sadar target mereka, dan menggunakan strategi dua tingkat mimpi ('mimpi dalam mimpi') untuk mengekstrak informasi penting yang dibutuhkan. Setiap ekstraktor membawa sebuah totem, benda pribadi yang berukuran kecil. Totem milik Cobb masih belum jelas namun ada kemungkinan bahwa cincin kawinnyalah yang menjadi totem miliknya, dikarenakan gasing yang ada di tangannya adalah totem milik istrinya. Cobb berjuang bersama memori istrinya yang telah meninggal, Mal (Marion Cotillard), yang memanifestasi mimpinya serta berusaha untuk menyabotase kehidupannya.
         * thumb_picture : [{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"}]
         * trailer_url : https://www.youtube.com/watch?v=8hP9D6kZseM
         * rating : 8.7
         * award : {"green":true,"red":false,"teal":true,"violet":false}
         * cast : [{"name_cast":"Leonardo DiCaprio","pic_cast":"https://image.ibb.co/hYfZjv/leonardo_dicaprio.jpg"},{"name_cast":"Tom Hardy","pic_cast":"https://image.ibb.co/ek8n4v/tom_hardy.jpg"},{"name_cast":"Ellen Page","pic_cast":"https://image.ibb.co/k5gwxF/ellen_page.jpg"},{"name_cast":"Joseph Gordon-Levitt","pic_cast":"https://image.ibb.co/iF7rWa/joshep_gordon.jpg"},{"name_cast":"Marion Cotillard","pic_cast":"https://image.ibb.co/mwg74v/marion_cotillard.jpg"}]
         */

        private String title;
        private String year;
        private String cover;
        private String genre;
        private String sinopsis;
        private String trailer_url;
        private String rating;
        private Award award;
        private List<ThumbPicture> thumb_picture;
        private List<Cast> cast;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getCover() {
            return cover;
        }

        public void setCover(String cover) {
            this.cover = cover;
        }

        public String getGenre() {
            return genre;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public String getSinopsis() {
            return sinopsis;
        }

        public void setSinopsis(String sinopsis) {
            this.sinopsis = sinopsis;
        }

        public String getTrailer_url() {
            return trailer_url;
        }

        public void setTrailer_url(String trailer_url) {
            this.trailer_url = trailer_url;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public Award getAward() {
            return award;
        }

        public void setAward(Award award) {
            this.award = award;
        }

        public List<ThumbPicture> getThumb_picture() {
            return thumb_picture;
        }

        public void setThumb_picture(List<ThumbPicture> thumb_picture) {
            this.thumb_picture = thumb_picture;
        }

        public List<Cast> getCast() {
            return cast;
        }

        public void setCast(List<Cast> cast) {
            this.cast = cast;
        }

        public static class Award {
            /**
             * green : true
             * red : false
             * teal : true
             * violet : false
             */

            private boolean green;
            private boolean red;
            private boolean teal;
            private boolean violet;

            public boolean isGreen() {
                return green;
            }

            public void setGreen(boolean green) {
                this.green = green;
            }

            public boolean isRed() {
                return red;
            }

            public void setRed(boolean red) {
                this.red = red;
            }

            public boolean isTeal() {
                return teal;
            }

            public void setTeal(boolean teal) {
                this.teal = teal;
            }

            public boolean isViolet() {
                return violet;
            }

            public void setViolet(boolean violet) {
                this.violet = violet;
            }
        }

        public static class ThumbPicture {
            /**
             * url_image : https://image.ibb.co/dqwDcF/images.jpg
             */

            private String url_image;

            public String getUrl_image() {
                return url_image;
            }

            public void setUrl_image(String url_image) {
                this.url_image = url_image;
            }
        }

        public static class Cast {
            /**
             * name_cast : Leonardo DiCaprio
             * pic_cast : https://image.ibb.co/hYfZjv/leonardo_dicaprio.jpg
             */

            private String name_cast;
            private String pic_cast;

            public String getName_cast() {
                return name_cast;
            }

            public void setName_cast(String name_cast) {
                this.name_cast = name_cast;
            }

            public String getPic_cast() {
                return pic_cast;
            }

            public void setPic_cast(String pic_cast) {
                this.pic_cast = pic_cast;
            }
        }
    }
}
