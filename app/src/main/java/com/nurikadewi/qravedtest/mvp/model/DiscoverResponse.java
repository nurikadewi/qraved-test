package com.nurikadewi.qravedtest.mvp.model;

import java.util.List;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class DiscoverResponse {

    /**
     * discover : {"trending_groups":{"title":"Trending Groups","list":[{"title_group":"Nature","icon_group":"https://image.ibb.co/kQVnnF/nature_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Arcade","icon_group":"https://image.ibb.co/mAFyuv/arcade_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Beer","icon_group":"https://image.ibb.co/iAOJuv/beer_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Action","icon_group":"https://image.ibb.co/kQVnnF/nature_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"}]},"interisting_users":{"title":"Interisting Users","picture":[{"url_picture":"https://image.ibb.co/k5gwxF/ellen_page.jpg"},{"url_picture":"https://image.ibb.co/hYfZjv/leonardo_dicaprio.jpg"},{"url_picture":"https://image.ibb.co/iF7rWa/joshep_gordon.jpg"},{"url_picture":"https://image.ibb.co/mwg74v/marion_cotillard.jpg"},{"url_picture":"https://image.ibb.co/ek8n4v/tom_hardy.jpg"}]},"personalized_content":{"title":"Personalized Content","picture":[{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"},{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"}]}}
     */

    private Discover discover;

    public Discover getDiscover() {
        return discover;
    }

    public void setDiscover(Discover discover) {
        this.discover = discover;
    }

    public static class Discover {
        /**
         * trending_groups : {"title":"Trending Groups","list":[{"title_group":"Nature","icon_group":"https://image.ibb.co/kQVnnF/nature_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Arcade","icon_group":"https://image.ibb.co/mAFyuv/arcade_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Beer","icon_group":"https://image.ibb.co/iAOJuv/beer_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Action","icon_group":"https://image.ibb.co/kQVnnF/nature_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"}]}
         * interisting_users : {"title":"Interisting Users","picture":[{"url_picture":"https://image.ibb.co/k5gwxF/ellen_page.jpg"},{"url_picture":"https://image.ibb.co/hYfZjv/leonardo_dicaprio.jpg"},{"url_picture":"https://image.ibb.co/iF7rWa/joshep_gordon.jpg"},{"url_picture":"https://image.ibb.co/mwg74v/marion_cotillard.jpg"},{"url_picture":"https://image.ibb.co/ek8n4v/tom_hardy.jpg"}]}
         * personalized_content : {"title":"Personalized Content","picture":[{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"},{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"}]}
         */

        private TrendingGroups trending_groups;
        private InteristingUsers interisting_users;
        private PersonalizedContent personalized_content;

        public TrendingGroups getTrending_groups() {
            return trending_groups;
        }

        public void setTrending_groups(TrendingGroups trending_groups) {
            this.trending_groups = trending_groups;
        }

        public InteristingUsers getInteristing_users() {
            return interisting_users;
        }

        public void setInteristing_users(InteristingUsers interisting_users) {
            this.interisting_users = interisting_users;
        }

        public PersonalizedContent getPersonalized_content() {
            return personalized_content;
        }

        public void setPersonalized_content(PersonalizedContent personalized_content) {
            this.personalized_content = personalized_content;
        }

        public static class TrendingGroups {
            /**
             * title : Trending Groups
             * list : [{"title_group":"Nature","icon_group":"https://image.ibb.co/kQVnnF/nature_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Arcade","icon_group":"https://image.ibb.co/mAFyuv/arcade_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Beer","icon_group":"https://image.ibb.co/iAOJuv/beer_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"},{"title_group":"Action","icon_group":"https://image.ibb.co/kQVnnF/nature_icon.png","picture_group":"https://image.ibb.co/dqwDcF/images.jpg"}]
             */

            private String title;
            private List<ListTrending> list;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<ListTrending> getList() {
                return list;
            }

            public void setList(List<ListTrending> list) {
                this.list = list;
            }

            public static class ListTrending {
                /**
                 * title_group : Nature
                 * icon_group : https://image.ibb.co/kQVnnF/nature_icon.png
                 * picture_group : https://image.ibb.co/dqwDcF/images.jpg
                 */

                private String title_group;
                private String icon_group;
                private String picture_group;

                public String getTitle_group() {
                    return title_group;
                }

                public void setTitle_group(String title_group) {
                    this.title_group = title_group;
                }

                public String getIcon_group() {
                    return icon_group;
                }

                public void setIcon_group(String icon_group) {
                    this.icon_group = icon_group;
                }

                public String getPicture_group() {
                    return picture_group;
                }

                public void setPicture_group(String picture_group) {
                    this.picture_group = picture_group;
                }
            }
        }

        public static class InteristingUsers {
            /**
             * title : Interisting Users
             * picture : [{"url_picture":"https://image.ibb.co/k5gwxF/ellen_page.jpg"},{"url_picture":"https://image.ibb.co/hYfZjv/leonardo_dicaprio.jpg"},{"url_picture":"https://image.ibb.co/iF7rWa/joshep_gordon.jpg"},{"url_picture":"https://image.ibb.co/mwg74v/marion_cotillard.jpg"},{"url_picture":"https://image.ibb.co/ek8n4v/tom_hardy.jpg"}]
             */

            private String title;
            private List<Picture> picture;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<Picture> getPicture() {
                return picture;
            }

            public void setPicture(List<Picture> picture) {
                this.picture = picture;
            }

            public static class Picture {
                /**
                 * url_picture : https://image.ibb.co/k5gwxF/ellen_page.jpg
                 */

                private String url_picture;

                public String getUrl_picture() {
                    return url_picture;
                }

                public void setUrl_picture(String url_picture) {
                    this.url_picture = url_picture;
                }
            }
        }

        public static class PersonalizedContent {
            /**
             * title : Personalized Content
             * picture : [{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"},{"url_image":"https://image.ibb.co/dqwDcF/images.jpg"},{"url_image":"https://image.ibb.co/cnRDcF/image_6.jpg"},{"url_image":"https://image.ibb.co/hRAH4v/image_7.jpg"},{"url_image":"https://image.ibb.co/f7Nara/images_8.jpg"},{"url_image":"https://image.ibb.co/i43RxF/images_5.jpg"},{"url_image":"https://image.ibb.co/fT7x4v/images_4.jpg"},{"url_image":"https://image.ibb.co/b50vra/images_3.jpg"},{"url_image":"https://image.ibb.co/kZSara/images_2.jpg"},{"url_image":"https://image.ibb.co/ijv6xF/images_1.jpg"}]
             */

            private String title;
            private List<PictureX> picture;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<PictureX> getPicture() {
                return picture;
            }

            public void setPicture(List<PictureX> picture) {
                this.picture = picture;
            }

            public static class PictureX {
                /**
                 * url_image : https://image.ibb.co/dqwDcF/images.jpg
                 */

                private String url_image;

                public String getUrl_image() {
                    return url_image;
                }

                public void setUrl_image(String url_image) {
                    this.url_image = url_image;
                }
            }
        }
    }
}
