package com.nurikadewi.qravedtest.application;

import android.app.Application;

import com.nurikadewi.qravedtest.di.components.DaggerIAppComponent;
import com.nurikadewi.qravedtest.di.components.IAppComponent;
import com.nurikadewi.qravedtest.di.module.AppModule;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class QravedTestApp extends Application {

    private IAppComponent mIAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeApplicationComponent();
    }

    private void initializeApplicationComponent() {
        mIAppComponent = DaggerIAppComponent
                .builder()
                .appModule(new AppModule(this, "http://private-bad99-qravedtest.apiary-mock.com"))
                .build();
    }

    public IAppComponent getApplicationComponent() {
        return mIAppComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
