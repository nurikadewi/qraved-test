package com.nurikadewi.qravedtest.api;

import com.nurikadewi.qravedtest.mvp.model.DiscoverResponse;
import com.nurikadewi.qravedtest.mvp.model.FollowerResponse;
import com.nurikadewi.qravedtest.mvp.model.MovieDetailResponse;

import retrofit2.http.GET;
import rx.Observable;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public interface IApiService {

    @GET("/follower")
    Observable<FollowerResponse> getFollowers();

    @GET("/detail_movie")
    Observable<MovieDetailResponse> getMovieDetail();

    @GET("/discover")
    Observable<DiscoverResponse> getDiscover();
}
