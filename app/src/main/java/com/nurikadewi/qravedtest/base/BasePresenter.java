package com.nurikadewi.qravedtest.base;

import com.nurikadewi.qravedtest.mvp.view.IBaseView;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/17/17.
 */

public class BasePresenter<V extends IBaseView> {

    @Inject
    protected V mView;

    protected V getView() {
        return mView;
    }

    protected <T> void subscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.newThread())
                .toSingle()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}
